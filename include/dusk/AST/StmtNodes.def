//===--- StmtNodes.def - Dusk statement metaprogramming ---------*- C++ -*-===//
//
//                                 dusk-lang
// This source file is part of a dusk-lang project, which is a semestral
// assignement for BI-PJP course at Czech Technical University in Prague.
// The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
//
// This file contains macros used for macro-metaprogramming with statements.
//
//===----------------------------------------------------------------------===//

/// STMT(Id, Parent)
///   For all non-abstract statement nodes, it's enumerator name is
///   \c StmtKind::Id. The node's class name is \c Id##Stmt, the name
///   of the base class is \c Parent.

#ifndef ABSTRACT_STMT
#define ABSTRACT_STMT(Id, Parent)
#endif

/// CONTEXT_STMT(Id, Parent)
///   Represents a statements, that causes context change such
///   as \c FuncStmt
#ifndef CONTEXT_STMT
#define CONTEXT_STMT(Id, Parent) STMT(Id, Parent)
#endif

/// CONTROL_STMT(Id, Parent)
///   Represents a control statements such as \c IfStmt.
#ifndef CONTROL_STMT
#define CONTROL_STMT(Id, Parent) CONTEXT_STMT(Id, Parent)
#endif

STMT(Break, Stmt)
STMT(Return, Stmt)
STMT(Subscript, Stmt)
STMT(Range, Stmt)
CONTEXT_STMT(Block, Stmt)
CONTEXT_STMT(Extern, Stmt)
CONTEXT_STMT(Func, Stmt)
CONTROL_STMT(For, Stmt)
CONTROL_STMT(While, Stmt)
CONTROL_STMT(If, Stmt)

#undef ABSTRACT_STMT
#undef CONTEXT_STMT
#undef CONTROL_STMT
#undef STMT
